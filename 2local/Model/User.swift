//
//  ResultData.swift
//  2Local
//
//  Created by Hasan Sedaghat on 9/1/19.
//  Copyright © 2019 Hasan Sedaghat. All rights reserved.
//

import UIKit

class User: Codable {
    var id: Int?
    var name:String?
    var privateKey:String?
    var publicKey:String?
    var userType:String?
    var email:String?
    var mobileNumber:Int?
    var image:String?
    var businessName:String?
    var webSite:String?
    var notes:String?
    var twofa:String?
    var firstName:String?
    var lastName:String?
    var birthday:String?
    var country:String?
    var city:String?
    var state:String?
    var postalCode:String?
    var address:String?
    var twofaStatus:String?
    var twofaSecret:String?
    var verificationSecret:String?
    var countryCode:Int?
    var mobileVerification:String?
    var mobileVerificationStatus:String?
    var balance: Double?
    var password:String?
    var currency:String?
    var apiToken:String?
    var accessToken:String?
    var tokenType:String?
    var affiliateCode: String?
    
    enum CodingKeys : String , CodingKey {
        case id = "id"
        case name = "name"
        case privateKey = "private_key"
        case publicKey = "public_key"
        case userType = "user_type"
        case email = "email"
        case mobileNumber = "mobile_number"
        case image = "image"
        case businessName = "business_name"
        case webSite = "website"
        case notes = "notes"
        case firstName = "first_name"
        case lastName = "last_name"
        case birthday = "birthday"
        case country = "country"
        case city = "city"
        case state = "state"
        case postalCode = "post_code"
        case address = "address"
        case twofa = "twofa"
        case twofaStatus = "twofa_status"
        case verificationSecret = "verification_secret"
        case countryCode = "country_code"
        case mobileVerification = "mobile_verification"
        case mobileVerificationStatus = "mobile_verification_status"
        //case balance = "balance"
        case apiToken = "api_token"
        case accessToken = "access_token"
        case tokenType = "token_type"
        case affiliateCode = "affiliate_code"
    }
    
//    required init(from decoder: Decoder) throws {
//        let container = try decoder.container(keyedBy: CodingKeys.self)
//        
//        let stringBalance = try? container.decode(String.self, forKey: .balance)
//        let intBalance = try? container.decode(Int.self, forKey: .balance)
//        self.balance = stringBalance ?? "\(intBalance ?? -1)"
//    }
}
