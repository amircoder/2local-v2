//
//  HomeVC+ETH.swift
//  2local
//
//  Created by Ebrahim Hosseini on 4/20/21.
//  Copyright © 2021 2local Inc. All rights reserved.
//

import Foundation
import KVNProgress

extension HomeViewController {
    
    /// get ETH balance
    func getETHBalance() -> String? {
        guard let wallet = DataProvider.shared.wallets.filter({$0.name == Coins.Ethereum}).first else { return nil }
        return wallet._balance
    }
    
    func getFiatOfETH(of balance: String) -> String {
        guard let wallet = DataProvider.shared.wallets.filter({$0.name == Coins.Ethereum}).first else { return "0" }
        return wallet.fiat(1)
    }
    
    func getETHTransaction() {
        guard let address = Web3Service.currentAddress else { return }
        APIManager.shared.getETHTransactionStatus(receiveAddress: address) { (data, response, error) in
            let result = APIManager.processResponseETHScan(response: response, data: data)
            if result.status {
                do {
                    let transaction = try JSONDecoder().decode(ResultData<[TransactionHistoryModel]>.self, from: data!).result
                    KVNProgress.dismiss()
                    if let data = transaction, data.count > 0 {
//                        self.mergeTransaction(data)
                    } else {
                        
                    }
                } catch {
                    DispatchQueue.main.async {
                        KVNProgress.showError(withStatus: "Failed to parse transaction history data")
                    }
                }
            } else {
                DispatchQueue.main.async {
                    KVNProgress.showError(withStatus: result.message)
                }
            }
        }
    }
    
    fileprivate func mergeTransaction(_ data: [TransactionHistoryModel]) {
        //TODO: - map ether transaction data to transaction and add to transaction
        data.forEach { (transaction) in
            let transaction = Transfer(id: Int(transaction.blockNumber ?? "0"),
                                       userId: 1,
                                       date: "\(transaction.timeStamp ?? "0")".toAppFormat(),
                                       status: transaction.confirmations,
                                       quantity: transaction.value,
                                       source: "In",
                                       wallet: "",
                                       amount: transaction.value,
                                       currency: nil)
            
            self.transfers.append(transaction)
        }
        
        DispatchQueue.main.async {
            self.chartCollectionView.reloadData()
        }
    }
    
}
