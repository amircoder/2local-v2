//
//  HomeViewController.swift
//  2local
//
//  Created by Hasan Sedaghat on 12/31/19.
//  Copyright © 2019 2local Inc. All rights reserved.
//

import UIKit
import web3swift
import KVNProgress

class HomeViewController: BaseVC {
    
    //MARK: - Oiutlets
    @IBOutlet weak var totalFiatValueLabel: UILabel!
    @IBOutlet weak var _2LocalbalanceLabel: UILabel!
    @IBOutlet weak var chartCollectionView: UICollectionView!
    @IBOutlet weak var walletCollectionView: UICollectionView!
    @IBOutlet weak var invisibleBTN: UIButton!
    @IBAction func unwindToHome(segue:UIStoryboardSegue) { }
    @IBOutlet weak var scrollView: UIScrollView!
    
    //MARK: - properties
    let months = Date().getLast12Month.0
    var transfers = DataProvider.shared.transfers
    var transactions = [TransactionChartModel]()
    var maxIncome : Float?
    var maxExpense : Float?
    var invisible = false
    
    private var wallets: [Wallets] = []
    private var walletBalance: String = "0"
    private var defaultSym: String = ""
    private var totalFiat: Double = 0
    private var totalfiatWithSymbol: String = "$0"
    var ethTransactionHistory: [TransactionHistoryModel] = []
    
    //MARK: - view cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCollection()
        
        generateWalets()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(generateWalets),
                                               name: Notification.Name.wallet,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(generateWalets),
                                               name: Notification.Name.walletRemove,
                                               object: nil)
        
        if UserDefaults.standard.bool(forKey: "invisible") {
            self.invisible = true
        } else {
            self.invisible = false
        }

        walletCollectionView.reloadData()
        
        showTransfer()
        
        setNavigation(title: "Total Balance", largTitle: true)
        
        let refreshControll = UIRefreshControl()
        refreshControll.addTarget(self, action: #selector(generateWalets), for: .valueChanged)
        scrollView.refreshControl = refreshControll
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        generateWalets()
        
        if invisible {
            if self._2LocalbalanceLabel.text !=  "******" {
                UIView.transition(with: self._2LocalbalanceLabel, duration: 0.5, options: .transitionFlipFromTop, animations: {
                    self._2LocalbalanceLabel.text = "******"
                }, completion: nil)
                
                UIView.transition(with: self.totalFiatValueLabel, duration: 0.5, options: .transitionFlipFromTop, animations: {
                    self.totalFiatValueLabel.text = "******"
                }, completion: nil)
            }
            invisibleBTN.setImage(#imageLiteral(resourceName: "eye1-selected"), for: .normal)
            UIView.animate(withDuration: 0.2) {
                self.view.layoutSubviews()
                self.view.layoutIfNeeded()
            }
        } else {
            if self._2LocalbalanceLabel.text != walletBalance {
                UIView.transition(with: self._2LocalbalanceLabel, duration: 0.5, options: .transitionFlipFromTop, animations: { [self] in
                    _2LocalbalanceLabel.text = walletBalance
                }, completion: nil)
                
            }
            if self.totalFiatValueLabel.text != totalfiatWithSymbol {
                UIView.transition(with: self.totalFiatValueLabel, duration: 0.5, options: .transitionFlipFromTop, animations: { [self] in
                    totalFiatValueLabel.text = totalfiatWithSymbol
                }, completion: nil)
            }
            
            UIView.animate(withDuration: 0.2) {
                self.view.layoutSubviews()
                self.view.layoutIfNeeded()
            }
        }
        
        walletCollectionView.reloadData()
    }
    
    //MARK: - functions
    fileprivate func showTransfer() {
        transfers = Transfer.calculateTransactions(transfers: transfers, orders: DataProvider.shared.orders).filter{($0.status == "paid" || $0.status == "Complete" || $0.status == "completed")}
        let months = Date().getLast12Month.1
        
        for month in months {
            let transaction = TransactionChartModel()
            transaction.date = month.prefix(7).description
            transactions.append(transaction)
        }
        
        for month in transactions {
            for transfer in transfers {
                if month.date == transfer.date?.prefix(7).description {
                    if transfer.source == "out" {
                        month.expenses +=  Float(transfer.quantity ?? "0.0")!
                    }
                    else {
                        month.income +=  Float(transfer.quantity ?? "0.0")!
                    }
                }
            }
        }
        
        maxIncome = transactions.map({$0.income}).max()
        maxExpense = transactions.map({$0.expenses}).max()
        chartCollectionView.reloadData()
    }
    
    @objc func generateWalets() {
        self.wallets.removeAll()
        self.wallets = DataProvider.shared.wallets
        self.defaultSym = DataProvider.shared.exchangeRate?.defaultSym ?? "$"
        self.getTotalFiat(self.wallets)
        self.walletCollectionView.reloadData()
    }
    
    fileprivate func getTotalFiat(_ wallets: [Wallets]) {
        self.totalFiat = 0
        guard wallets.count > 0 else { return }
        wallets.forEach { (wallet) in
            let currentWallet = WalletFactory.getWallets(wallet: wallet)
            let balance = Double(currentWallet.balance())
            currentWallet.fiat(from: balance) { (fiat) in
                self.totalFiat += Double(fiat)!
                DispatchQueue.main.async {
                    self.totalfiatWithSymbol = self.defaultSym + "\(self.totalFiat)".convertToPriceType()
                }
            }
        }
    }
    
    //MARK: - actions
    @IBAction func goToSettings(_ sender: Any) {
        let vc = UIStoryboard.settings.instantiate(viewController: SettingsViewController.self)
        let navc = TLNavigationController(rootViewController: vc)
        present(navc, animated: true)
    }
    
    @IBAction func goToNotification(_ sender: Any) {
        let vc = UIStoryboard.notification.instantiate(viewController: NotificationViewController.self)
        let navc = TLNavigationController(rootViewController: vc)
        present(navc, animated: true)
    }
    
    @IBAction func goToTransaction(_ sender: Any) {
        let vc = UIStoryboard.home.instantiate(viewController: TransactionsViewController.self)
        if let navigation = navigationController {
            navigation.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func invisibleAmounts(_ sender: Any) {
        if !invisible {
            UIView.transition(with: self._2LocalbalanceLabel, duration: 0.5, options: .transitionFlipFromTop, animations: {
                self._2LocalbalanceLabel.text = "******"
            }, completion: nil)
            
            
            UIView.transition(with: self.totalFiatValueLabel, duration: 0.5, options: .transitionFlipFromTop, animations: {
                self.totalFiatValueLabel.text = "******"
            }, completion: nil)
            invisibleBTN.setImage(#imageLiteral(resourceName: "eye1-selected"), for: .normal)
            UIView.animate(withDuration: 0.2) {
                self.view.layoutSubviews()
                self.view.layoutIfNeeded()
            }
        } else {
            if self._2LocalbalanceLabel.text != walletBalance {
                UIView.transition(with: self._2LocalbalanceLabel, duration: 0.5, options: .transitionFlipFromTop, animations: { [self] in
                    _2LocalbalanceLabel.text = walletBalance
                }, completion: nil)
                
            }
            if self.totalFiatValueLabel.text != totalfiatWithSymbol {
                UIView.transition(with: self.totalFiatValueLabel, duration: 0.5, options: .transitionFlipFromTop, animations: { [self] in
                    totalFiatValueLabel.text = totalfiatWithSymbol
                }, completion: nil)
            }
            invisibleBTN.setImage(#imageLiteral(resourceName: "eyeFill"), for: .normal)
            UIView.animate(withDuration: 0.2) {
                self.view.layoutSubviews()
                self.view.layoutIfNeeded()
            }
        }
        invisible.toggle()
        UserDefaults.standard.set(invisible, forKey: "invisible")
        walletCollectionView.reloadData()
    }
    
}


//MARK: - UICollectionView
extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func setupCollection() {
        walletCollectionView.delegate = self
        walletCollectionView.dataSource = self
        walletCollectionView.isPagingEnabled = true
        walletCollectionView.showsHorizontalScrollIndicator = false
        walletCollectionView.contentInset.left = 16
        
        chartCollectionView.delegate = self
        chartCollectionView.dataSource = self
        
        walletCollectionView.register(UINib(nibName: "AddNewWalletCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "AddNewWalletCollectionViewCell")//(AddNewWalletCollectionViewCell.self)
        walletCollectionView.register(WalletCollectionViewCell.self)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == walletCollectionView {
            return wallets.count + 1
        } else {
            return 12
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == walletCollectionView {
            let row = indexPath.row
            if row == wallets.count {
                let cell = walletCollectionView.dequeue(AddNewWalletCollectionViewCell.self, indexPath: indexPath)
                cell.addNewWalletButtonCallback = { [weak self] in
                    guard let self = self else { return }
                    let vc = UIStoryboard.wallet.instantiate(viewController: AddNewWalletVC.self)
                    vc.initwith(self.wallets)
                    let navc = TLNavigationController(rootViewController: vc)
                    if let navigation = self.navigationController {
                        navigation.present(navc, animated: true)
                    }
                }
                return cell
            } else {
                let cell = walletCollectionView.dequeue(WalletCollectionViewCell.self, indexPath: indexPath)
                
                let wallet = self.wallets[row]
                
                cell.fill(wallet, invisible: invisible)
                
                cell.buyButtonCallback = { [weak self] in
                    guard let self = self else { return }
                    let vc = UIStoryboard.main.instantiate(viewController: Buy2LCViewController.self)
                    if let navigation = self.navigationController {
                        navigation.pushViewController(vc, animated: true)
                    }
                }
                
                cell.receiveButtonCallback = { [weak self] in
                    guard let self = self else { return }
                    let vc = UIStoryboard.main.instantiate(viewController: ReceiveViewController.self)
                    vc.initWith(wallet)
                    if let navigation = self.navigationController {
                        navigation.pushViewController(vc, animated: true)
                    }
                }
                
                cell.sendButtonCallback = { [weak self] in
                    guard let self = self else { return }
                    let vc = UIStoryboard.transaction.instantiate(viewController: SendViewController.self)
                    vc.initWith(wallet)
                    if let navigation = self.navigationController {
                        navigation.pushViewController(vc, animated: true)
                    }
                }
                
                return cell
            }
        } else {
            let cell = chartCollectionView.dequeueReusableCell(withReuseIdentifier: "transactionCell", for: indexPath) as! TransactionChartCollectionViewCell
            cell.monthLabel.text = months[indexPath.row]
            let maxIncomeHeight = cell.barView.frame.height / 2
            let maxExpenseHeight = cell.barView.frame.height / 2
            let maxVal = max(self.maxIncome!, self.maxExpense!)
            
            UIView.animate(withDuration: 0, animations: {
                cell.incomeHeightConst.constant = 0
                cell.expenseHeightConst.constant = 0
                cell.barView.alpha = 0
                cell.indicatorView.alpha = 1
            }) { (finish) in
                cell.barView.alpha = 1
                cell.indicatorView.alpha = 0
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2) {
                    cell.barView.alpha = 1
                    cell.indicatorView.alpha = 0
                    UIView.animate(withDuration: 1, delay: 0, options: [.curveEaseInOut], animations: {
                        cell.incomeHeightConst.constant = (CGFloat(self.transactions[indexPath.row].income) * maxIncomeHeight / CGFloat(maxVal))
                        cell.expenseHeightConst.constant =  (CGFloat(self.transactions[indexPath.row].expenses) * maxExpenseHeight / CGFloat(maxVal))
                        if ((0...1).contains(cell.incomeHeightConst.constant) && (0...1).contains(cell.expenseHeightConst.constant ) || self.transfers.count == 0){
                            cell.barView.alpha = 0
                            cell.indicatorView.alpha = 1
                        }
                        if (0...1).contains(cell.incomeHeightConst.constant) {
                            cell.incomeHeightConst.constant = 4
                        }
                        if (0...1).contains(cell.expenseHeightConst.constant) {
                            cell.expenseHeightConst.constant = 4
                        }
                        cell.barView.layoutIfNeeded()
                    }) { (finish) in
                        
                    }
                }
            }
            return cell
        }
    }
}

//MARK: - CollectionView FlowLayout Delegate
extension HomeViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == walletCollectionView {
            return CGSize(width: UIScreen.main.bounds.width - 40, height: WalletCollectionViewCell.getHeight())
        } else {
            return CGSize(width: chartCollectionView.frame.width / 12, height: chartCollectionView.frame.height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        .zero
    }
    
}
