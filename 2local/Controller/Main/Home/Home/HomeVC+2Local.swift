//
//  HomeVC+2Local.swift
//  2local
//
//  Created by Ebrahim Hosseini on 4/20/21.
//  Copyright © 2021 2local Inc. All rights reserved.
//

import Foundation

extension HomeViewController {
    func get2localBalance() {
        
    }
    
    func get2localPublickey() -> String {
        guard let publicKey = DataProvider.shared.user?.publicKey else { return "" }
        return  publicKey
    }
}
