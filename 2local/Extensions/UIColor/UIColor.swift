//
//  UIColor.swift
//  2local
//
//  Created by Ebrahim Hosseini on 3/23/21.
//  Copyright © 2021 2local Inc. All rights reserved.
//
import UIKit


extension UIColor {
    convenience init(r: CGFloat, g: CGFloat, b: CGFloat, alpha: CGFloat = 1) {
        self.init(red: r / 255, green: g / 255, blue: b / 255, alpha: alpha)
    }
}

extension UIColor {
    @nonobjc class var _002CA4: UIColor {
        return UIColor(named: "002CA4")!
    }
    
    @nonobjc class var _000372: UIColor {
        return UIColor(named: "000372")!
    }
    
    @nonobjc class var _303030: UIColor {
        return UIColor(named: "303030")!
    }
    
    @nonobjc class var _404040: UIColor {
        return UIColor(named: "404040")!
    }
    
    @nonobjc class var _575757: UIColor {
        return UIColor(named: "575757")!
    }
    
    @nonobjc class var _606060: UIColor {
        return UIColor(named: "606060")!
    }
    
    @nonobjc class var _919191: UIColor {
        return UIColor(named: "919191")!
    }
    
    @nonobjc class var _EBEBEB: UIColor {
        return UIColor(named: "EBEBEB")!
    }
    
    @nonobjc class var _F2F2F8: UIColor {
        return UIColor(named: "F2F2F8")!
    }
    
    @nonobjc class var _F6F6F9: UIColor {
        return UIColor(named: "F6F6F9")!
    }
    
    @nonobjc class var _FFFFFF60: UIColor {
        return UIColor(named: "FFFFFF60")!
    }
    
    @nonobjc class var _flamenco: UIColor {
        return UIColor(named: "flamenco")!
    }
    
    @nonobjc class var _gainsboro: UIColor {
        return UIColor(named: "gainsboro")!
    }
    
    @nonobjc class var _blueHaze: UIColor {
        return UIColor(named: "blueHaze")!
    }
    
    @nonobjc class var _solitude: UIColor {
        return UIColor(named: "solitude")!
    }
    
    @nonobjc class var _sorbus: UIColor {
        return UIColor(named: "sorbus")!
    }
    
    @nonobjc class var _topaz: UIColor {
        return UIColor(named: "topaz")!
    }
    
    @nonobjc class var _shamrock: UIColor {
        return UIColor(named: "shamrock")!
    }
    
    @nonobjc class var _bittersweet: UIColor {
        return UIColor(named: "bittersweet")!
    }
    
    @nonobjc class var _mediumSlateBlue: UIColor {
        return UIColor(named: "mediumSlateBlue")!
    }
    
    @nonobjc class var _lightSlateBlue: UIColor {
        return UIColor(named: "lightSlateBlue")!
    }
    
    @nonobjc class var _logan: UIColor {
        return UIColor(named: "logan")!
    }
    
    @nonobjc class var _darkGray: UIColor {
        return UIColor(named: "darkGray")!
    }
    
    @nonobjc class var _mortar: UIColor {
        return UIColor(named: "mortar")!
    }
    
    @nonobjc class var _linkWater: UIColor {
        return UIColor(named: "linkWater")!
    }
    
    @nonobjc class var _E0E0EB: UIColor {
        return UIColor(named: "E0E0EB")!
    }
    
    @nonobjc class var _202020: UIColor {
        return UIColor(named: "202020")!
    }
    
    @nonobjc class var _707070: UIColor {
        return UIColor(named: "707070")!
    }
    
    @nonobjc class var _EF8749: UIColor {
        return UIColor(named: "EF8749")!
    }
    
    @nonobjc class var _9796AE: UIColor {
        return UIColor(named: "9796AE")!
    }
    
    @nonobjc class var _FE6C6C: UIColor {
        return UIColor(named: "FE6C6C")!
    }
    
    
}
