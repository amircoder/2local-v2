//
//  DataProvider.swift
//  2local
//
//  Created by Hasan Sedaghat on 12/28/19.
//  Copyright © 2019 2local Inc. All rights reserved.
//

import Foundation
import KeychainSwift

class DataProvider: NSObject {
    static let shared = DataProvider()
    
    var wallets: [Wallets] = []
    let keychain = KeychainSwift()
    var user: User?
    var exchangeRate: ExchangeRate?
    var defaultEx : String?
    var orders = [Order]()
    var transfers = [Transfer]()
    var transactions = [Transfer]()
    let issuer = "GBM3YRHLGJHKE2HIXUKJPJQBROTB57NSDXY5H5BCKX7JEOX5O4S5PKXL"
    let issuingKeys = "SDJPZNZBXURHTMUW2H26F2OTPPKU4ZAPONVTISKO35LMALVASPMEVFPP"
    let sourceKey = "GBM3YRHLGJHKE2HIXUKJPJQBROTB57NSDXY5H5BCKX7JEOX5O4S5PKXL"
    let publicKey = "GAISGT2MPE37FAIZETGZCCVU5TPGSHNAOBSIFYKSNGKRWNLOIYXPQFIO"
    let stellarWalletNumber = "GAPCSW7SIUH6ZY2J3Y3G2QTC2D6TD4G5FFN7BNHXALVQW24OJ2PXCFMF"
    let etheriumWalletNumber = "0xC5D646e1c989135A0e3BF0C021EdD386B57e0B26"
    let bitcoinWalletNumber = "1FMXXUATgkAP3ZdPgNBQcxsvjw1YLvFDBr"
    
//    var places = [Place(id: 0, name: "Global Edutainment", tel:"+31 252 518 537 , +31 654 39 85 21", address: "Wilsonweg 2, 2182LR Hillegom, Netherlands", lat: 52.285294, lng: 4.575433,websiteURL: "asd")]
    var places = [Companies]()
}

